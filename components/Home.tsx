import WebView, {WebViewMessageEvent} from "react-native-webview";
import {View, Text, FlatList} from "react-native";
import {useEffect, useState} from "react";
import { Feather, MaterialCommunityIcons, AntDesign } from '@expo/vector-icons';
import * as Location from 'expo-location';
import {LocationObject} from "expo-location";

export default function Home() {
    const [location, setLocation] = useState<LocationObject>();
    const [errorMsg, setErrorMsg] = useState(null);
    const [panelClosed, setPanelClosed] = useState(true);
    const [line, setLine] = useState("");
    const [stop, setStop] = useState<any>();
    const [steps, setSteps] = useState<any>([{}]);
    let WebViewRef: WebView;

    useEffect(() => {
        (async () => {
            let { status } = await Location.requestForegroundPermissionsAsync();
            if (status !== 'granted') {
                console.log('Permission to access location was denied');
                return;
            }
            let location = await Location.getCurrentPositionAsync({});
            const jsonLocation =  JSON.stringify(location);
            WebViewRef.injectJavaScript(`window.geo(`+jsonLocation+`)`);
            setLocation(location);
        })();
    }, []);

    const onMessage = (event: WebViewMessageEvent) => {
        let data = JSON.parse(event.nativeEvent.data);
        const label = data.label;
        data = data.data;
        switch (label) {
            case "steps":
                let directions = [];
                for (const step of data) {
                    if (step.distance == 0 && step.instruction.type == "depart" || step.instruction.type == "arrive") {
                        directions.push({
                            message: "Prendre le " + stop?.type + " " + line + " à l'arrêt " + stop?.name,
                            iconName: "bus-stop",
                            icon: "MaterialCommunityIcons"
                        });
                    } else if (step.instruction.type == "depart") {
                        let msgDepart = "Départ";
                        if (step.attributes.name.nom_1_gauche != "") {
                            msgDepart += " sur ";
                        }
                        directions.push({
                            message: msgDepart + step.attributes.name.nom_1_gauche + " (" + step.distance + "m)",
                            iconName: "map-marker",
                            icon: "MaterialCommunityIcons"
                        });
                    } else {
                        let message = "";
                        let iconName = "";
                        if (step.instruction.type == "continue" || step.instruction.modifier == "straight") {
                            message = "Continuer tout droit sur ";
                            iconName = "arrow-up";
                        } else {
                            switch(step.instruction.modifier) {
                                case "sharp right":
                                case "right":
                                    message = "Tourner à droite sur ";
                                    iconName = "corner-up-right";
                                    break;
                                case "slight right":
                                    message = "Tourner légèrement à droite sur ";
                                    iconName = "arrow-up-right";
                                    break;
                                case "sharp left":
                                case "left":
                                    message = "Tourner à gauche sur ";
                                    iconName = "corner-up-left";
                                    break;
                                case "slight left":
                                    message = "Tourner légèrement à gauche sur ";
                                    iconName = "arrow-up-left";
                            }
                        }
                        message += step.attributes.name.nom_1_gauche + " (" + step.distance + "m)";
                        directions.push({
                            message,
                            iconName,
                            icon: "Feather"
                        });
                    }
                }
                setSteps(directions);
                setPanelClosed(true);
                break;
            case "line":
                setLine(data.substring(1));
                break;
            default:
                setStop(data);
        }
    }

    const html = `<!doctype html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.10.0/css/ol.css" type="text/css">
        <style>
            .map {
                height: 100vh;
                width: 100vw;
            }

            .wrapper {
                display: grid;
                grid-template-columns: repeat(4, 5em);
            }

            .modal {
                display: none; /* Hidden by default */
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            }

            /* Modal Content/Box */
            .modal-content {
                background-color: #fefefe;
                margin: 15% auto; /* 15% from the top and centered */
                padding: 20px;
                border: 1px solid #888;
                width: 85%; /* Could be more or less, depending on screen size */
            }
            
            input[type=checkbox] {
                margin-right: 5px;
                margin-bottom: 5px;
            }

            #filtreBtn{
                margin-top: 1.5em;
                margin-bottom: 5px;
            }

            .checkbox {
                margin-bottom: 1em;
            }
            
            #valideBtn{
                margin: 10px;
            }
            
            .ol-popup {
                position: absolute;
                background-color: white;
                box-shadow: 0 1px 4px rgba(0,0,0,0.2);
                padding: 8px;
                border-radius: 10px;
                border: 1px solid #cccccc;
                bottom: 8px;
                left: -40px;
                min-width: 180px;
                text-align: center;
            }
            
            .ol-popup:after, .ol-popup:before {
                top: 100%;
                border: solid transparent;
                content: " ";
                height: 0;
                width: 0;
                position: absolute;
                pointer-events: none;
                text-align: center;
            }
            .ol-popup:after {
                border-top-color: white;
                border-width: 10px;
                left: 48px;
                margin-left: -10px;
                text-align: center;
            }
            .ol-popup:before {
                border-top-color: #cccccc;
                border-width: 11px;
                left: 48px;
                margin-left: -11px;
                text-align: center;
            }
            .ol-popup-closer {
                text-decoration: none;
                position: absolute;
                top: 2px;
                right: 8px;
                padding: 1px;
                padding-left:3px;
            }
            .ol-popup-closer:after {
                content: "✖";
                font-size: 1px;
            }
            .div_centre{
                display: flex;
                justify-content: center;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.10.0/build/ol.js"></script>
        <title>OpenLayers example</title>
    </head>
    <body>
        <button id="filtreBtn">Filtre</button>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <h3>Trams</h3>
                <div id="checkboxTram" class="wrapper" ></div>
                <h3>Lignes de bus</h3>
                <div id="checkboxLigne" class="wrapper"></div>
                <h3>Stationnements de vélo</h3>
                <div id="checkboxParkingVelo" class="wrapper">
                </div>
                <div class="checkbox">
                    <input type="checkbox" id="pistesCyclables">
                    <label>Pistes Cyclables</label>
                </div>
                <div class="div_centre"><button  id ="valideBtn" onclick="update()">Valider</button></div>
            </div>
        </div>

        <input type="text" id="destination" />
        <button type="button" id="searchButton" onclick="findPath()">OK</button>
        <div id="map" class="map"></div>
        <div id="popup" class="ol-popup">
            <a href="#" id="popup-closer" class="ol-popup-closer"></a>
            <div id="popup-content"></div>
        <script type="text/javascript">
            // CODE QUESTION 4
                
            // Elements Html
            var modal = document.getElementById("myModal");
            var filtreBtn = document.getElementById("filtreBtn");
            var checkboxLigne = document.getElementById('checkboxLigne');
            var checkboxTram = document.getElementById('checkboxTram');
            var checkboxParkingVelo = document.getElementById('checkboxParkingVelo');

            // Elements pour popup des arrêts
            const container = document.getElementById('popup');
            const content = document.getElementById('popup-content');
            const closer = document.getElementById('popup-closer');

            // Variables globales
            var espacesTravail = 'TCO';
            var urlGeoserverSansWNS = 'https://pdicost.univ-orleans.fr/pghost/geoserver/'+ espacesTravail 
            var urlGeoserver = urlGeoserverSansWNS +'/wms';
            const listeBus = ["L01", "L02","L03","L04","L05","L06","L07","L08","L09","L11","L12","L13","L15","L16","L17","L18","L19","L20","L21","L22","L23","L25","L26","L27","L28","L33","L34","L35","L36","L37","L60","L61","L62","L63","L64","L65","L70","L71","L72","L73","LA","LB","LIC","LL", "LO"]
            const listeTram = ["A","B"]
            const listeParkingVelo = ["Arceaux", "Abri", "Parc Relais", "Rateaux", "Véloparc", "Vélo Plus", "Ouvrage", "Porte Vélo"]
            let position_user = [1.90169789941, 47.8979002426];

            var layer= new ol.layer.Tile({
                source: new ol.source.OSM()
            });
            
            const overlay = new ol.Overlay({
                element: container,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250,
                    },
            });

            const view = new ol.View({
                center: ol.proj.fromLonLat([1.909251, 47.902964]),
                zoom: 11
            });
            
            var map=new ol.Map({
                layers: [layer],
                target:'map',
                overlays: [overlay],
                view: view
            });
            

            //Layer ligne de tram
            var layerTram = new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: urlGeoserver,
                    params: {'LAYERS': espacesTravail+':tao_ligne_tram_od', 'CQL_FILTER':"num_ligne=''",'VERSION':'1.1.1'},
                    serverType: 'geoserver'
                })});
            var paramLayerTram = layerTram.getSource().getParams();

            //Layer des arrets
            var layerArret = new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: urlGeoserver,
                    params: {'LAYERS': espacesTravail+':tao_arrets_od','CQL_FILTER':"lignes_pas NOT LIKE '%%'"},
                    serverType: 'geoserver'
                })});
            var paramLayerArret = layerArret.getSource().getParams();

            //Layer ligne de bus
            var layerLigne = new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: urlGeoserver,
                    params: {'LAYERS': espacesTravail+':tao_ligne_bus_od','CQL_FILTER':"num_ligne=''", 'VERSION':'1.1.1'},
                    serverType: 'geoserver'
                })});
            var paramLayerLigne = layerLigne.getSource().getParams();

            // Layer pistes cyclables
            var layerPistesCyclables = new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: urlGeoserver,
                    params: {'LAYERS': espacesTravail+':amenagements-cyclables', 'VERSION':'1.1.1'},
                    serverType: 'geoserver'
                })});
            layerPistesCyclables.setVisible(false);
            
            // Layers stationnements vélo
            for (const type of listeParkingVelo) {
                const regex = type.replace(/é/g, "%");
                let layerName;
                let filtered = false;
                switch (type) {
                    case "Parc Relais":
                        layerName = ":parcs-relais-velos-securises-tao-2018-orleans-metropole";
                        break;
                    case "Vélo Plus":
                        layerName = ":liste-des-stations-velo-2018-orleans-metropole";
                        break;
                    case "Arceaux":
                        layerName = ":om-mobilite-poi-arceauxstationnementvelo";
                        break;
                    default:
                        layerName = ":referentielbdauao_dep_station_velos";
                        filtered = true;
                }
                let params;
                if (filtered) {
                    params = {'LAYERS': espacesTravail+ layerName, 'STYLES': type, 'CQL_FILTER': "type LIKE '" + regex + "'", 'VERSION':'1.1.1'};
                } else {
                    params = {'LAYERS': espacesTravail+ layerName, 'STYLES': type, 'VERSION':'1.1.1'};
                }
                let layer = new ol.layer.Tile({
                    source: new ol.source.TileWMS({
                        url: urlGeoserver,
                        params: params,
                        serverType: 'geoserver'
                    })});
                layer.set('name', type)
                layer.setVisible(false);
                map.addLayer(layer);
            }
                
            //Ajouter layers
            map.addLayer(layerTram);
            map.addLayer(layerLigne);
            map.addLayer(layerArret);
            map.addLayer(layerPistesCyclables);

            // Créer un checkbox
            function createCheckbox(id) {
                let newCheckbox = document.createElement("input");
                newCheckbox.type = "checkbox";
                newCheckbox.checked = false;
                newCheckbox.id=id;
                return newCheckbox;
            }

            // Init checkbox des lignes de bus
            for (var i = 0; i < listeBus.length; i++) {
                // Creation checkbox
                const newCheckbox = createCheckbox(listeBus[i]);

                // Creation label: le nom du bus
                var label = document.createElement('label')
                label.appendChild(document.createTextNode(listeBus[i].slice(1, 3)));

                //Creation div => checkbox + label
                var div = document.createElement('div');
                div.classList.add("checkbox")
                div.appendChild(newCheckbox);
                div.appendChild(label)

                checkboxLigne.appendChild(div);
            }

            // Init checkbox des lignes de tram
            for (var i = 0; i < listeTram.length; i++) {
                // Creation checkbox
                let newCheckbox = createCheckbox(listeTram[i]);
                newCheckbox.arret =  "T" + listeTram[i];

                // Creation label: le nom du tram
                var label = document.createElement('label')
                label.appendChild(document.createTextNode(listeTram[i]));

                //Creation div => checkbox + label
                var div = document.createElement('div');
                div.classList.add("checkbox")
                div.appendChild(newCheckbox);
                div.appendChild(label)

                checkboxTram.appendChild(div);
            }
            
            // Init checkbox des stationnements vélo
            for (let i = 0; i < listeParkingVelo.length; i++) {
                // Creation checkbox
                const newCheckbox = createCheckbox(listeParkingVelo[i]);

                // Creation label: le nom du type de stationnement
                let label = document.createElement('label')
                label.appendChild(document.createTextNode(listeParkingVelo[i]));

                //Creation div => checkbox + label
                let div = document.createElement('div');
                div.classList.add("checkbox")
                div.appendChild(newCheckbox);
                div.appendChild(label)

                checkboxParkingVelo.appendChild(div);
            }

            // Fonction de mise à jour de la carte en fonvtion du fitre
            function update(){
                resLigne = "";
                resTram = "";
                resArret = "";
                checkLigne= false
                checkTram= false

                // Faire cql pour les ligne de bus
                var liste = document.getElementById("checkboxLigne").childNodes;
                for (i = 0; i < liste.length; i++) {
                    var checked =liste[i].childNodes[0]
                    if(checked.checked == true){
                        checkLigne = true
                        if (resLigne.length != 0){
                            resLigne += " or "
                            resArret += " or "
                        }
                        resLigne += "num_ligne='"+checked.id+"'"
                        resArret += "lignes_pas LIKE '%"+checked.id+"%'"
                    }
                }
                if(!checkLigne){
                    resLigne += "num_ligne=''"
                }

                // Faire cql pour les ligne de tram
                var liste = document.getElementById("checkboxTram").childNodes;
                for (i = 0; i < liste.length; i++) {
                    var checked =liste[i].childNodes[0]
                    if(checked.checked == true){
                        checkTram = true
                        if (resTram.length != 0){
                            resTram += " or "
                            resArret += " or "
                        }
                        else if(checkLigne){
                            resArret += " or "
                        }
                        resTram += "num_ligne='"+checked.id+"'"
                        resArret += "lignes_pas LIKE '%"+checked.arret+"%'"
                    }
                }
                if(!checkTram){
                    resTram += "num_ligne=''"
                }
                if (!checkTram && !checkLigne){
                    resArret += "lignes_pas NOT LIKE '%%'"
                }
                

                // afficher layers stationnements de vélo si cochés
                for (let layer of map.getLayers().getArray()) {
                    if (listeParkingVelo.includes(layer.get('name'))) {
                        layer.setVisible(document.getElementById(layer.get('name')).checked);
                    }
                }

                // afficher layer pistes cyclables si coché
                layerPistesCyclables.setVisible(document.getElementById("pistesCyclables").checked);


                //Mise à jour des layer en changent le filtre cql
                paramLayerLigne.CQL_FILTER = resLigne;
                layerLigne.getSource().updateParams(paramLayerLigne);

                paramLayerTram.CQL_FILTER = resTram;
                layerTram.getSource().updateParams(paramLayerTram);

                paramLayerArret.CQL_FILTER = resArret ;
                layerArret.getSource().updateParams(paramLayerArret);

                //Fermé la pop-up des filtres
                modal.style.display = "none";
            }

            // Action ouvrir la pop-up des filtres
            filtreBtn.onclick = function() {
                modal.style.display = "block";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }

            
            //Fonction ajouter le point de la geolocation
            function geo(geolocPosition) {
                // Creation du point 
                position_user = [geolocPosition.coords.longitude, geolocPosition.coords.latitude]
                var point = new ol.source.Vector({features: [new ol.Feature({
                        geometry: new ol.geom.Point(ol.proj.fromLonLat(position_user)),
                    })]});


                // Creation du style
                var pointStyle = new ol.style.Style(
                    {image: new ol.style.Circle(
                    {radius: 10,fill: new ol.style.Fill({color: '#3399CC',}),
                        stroke: new ol.style.Stroke({color: '#fff',width: 2,}),
                    })});
                var pointLayer = new ol.layer.Vector({
                    source: point,style: pointStyle
                });

                map.addLayer(pointLayer);
            }
            // CODE QUESTION 5
            
            // ajouter le layer des commentaires depuis le postgis
            var layerCom = new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: urlGeoserver,
                    params: {'LAYERS': 'TCO:commentaire', 'VERSION':'1.1.1'},
                    serverType: 'geoserver'
            })});
            map.addLayer(layerCom);
            
            // apparition des commentaires en fonction du zoom
            var currZoom = map.getView().getZoom();
            map.on('moveend', function(e) {
              var newZoom = map.getView().getZoom();
              if (currZoom != newZoom) {
                console.log('zoom end, new zoom: ' + newZoom);
                currZoom = newZoom;
                if (newZoom >= 11){
                    layerCom.setVisible(true);
                }
                else {
                    layerCom.setVisible(false);
                }
              }
            });

            // CODE QUESTION 6
            
            const vectorSource = new ol.source.Vector();
            const vectorLayer = new ol.layer.Vector({source: vectorSource});
            map.addLayer(vectorLayer);
            
            const featureEndStop = new ol.Feature({
                geometry: new ol.geom.Point([0,0])
            });
            featureEndStop.setStyle(new ol.style.Style(null));
            vectorSource.addFeature(featureEndStop);
            let endStopLocation = [];

            const url = urlGeoserverSansWNS +"/ows?service=wfs&version=1.1.1&request=getfeature&typename=";
        
            async function findPath() {
                // supprime les accents, remplace les apostrophes par '%', trim et le transforme en majuscule
                const destination = document.getElementById("destination").value.normalize("NFD").replace(/\\p{Diacritic}/gu, "").replace(/'|’/g, "%25").trim().toUpperCase();
                // récupère les lignes passant par l'arrêt de destination
                const request = url + "tco:tao_arrets_od&CQL_FILTER=nom_generi LIKE '" + destination + "'+AND+(categ_arret='BUS'+OR+categ_arret='TRAM')&outputformat=application/json";
                fetch(request).then(function(response) {
                    if(response.ok) {
                        response.json().then(function(data) {
                            if (data.features.length == 0) {
                                alert("Aucun résultat pour votre recherche")
                            } else {
                                endStopLocation = data.features[0].geometry.coordinates;
                                featureEndStop.getGeometry().setCoordinates(ol.proj.fromLonLat(endStopLocation));
                                featureEndStop.set('name', data.features[0].properties.nom_generi)

                                let lines = "";
                                for (const feature of data.features) {
                                    lines += feature.properties.lignes_pas + " / ";
                                }
                                const filteredLines = [...new Set(lines.split(' / '))].filter(l => !l.includes("RESA") && l != "" && l != "" && !l.includes("LA") && !l.includes("LB"));
                                getNearestStartStop(filteredLines);
                            }
                        });
                    } else {
                        alert('Erreur ' + response.status);
                    }
                })
                    .catch(function(error) {
                        alert('Il y a eu un problème avec l\\'opération fetch: ' + error.message);
                    });
            }

            const featureStartStop = new ol.Feature({
                geometry: new ol.geom.Point([0,0])
            });
            featureStartStop.setStyle(new ol.style.Style(null));
            vectorSource.addFeature(featureStartStop);
            
            async function getNearestStartStop(lines) {
                let filter = "";
                for (const line of lines) {
                    filter += "lignes_pas%20LIKE%20%27%25" + line + "%25%27+OR+"
                }
                if (filter !== "")
                    filter = filter.substring(0, filter.length - 4) + "+AND+(categ_arret='BUS'+OR+categ_arret='TRAM')";
                // récupère tous les arrêts des lignes passant par l'arrêt de destination
                const request = url + "tco:tao_arrets_od&CQL_FILTER="+ filter +"&outputformat=application/json&srsname=EPSG:4326&EPSG:4326";
                fetch(request).then(function(response) {
                    if(response.ok) {
                        response.json().then(function(data) {
                            const geojsonFormat = new ol.format.GeoJSON({dataProjection: 'EPSG:4326'});
                            // reads and converts GeoJSon to Feature Object
                            const features = geojsonFormat.readFeatures(data, {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:4326'
                            });
                            let minDistance = Infinity;
                            let closestFeature = null;
                            for (const feature of features) {
                                let lineString = new ol.geom.LineString([position_user, feature.getGeometry().getCoordinates()]);
                                let distance = ol.sphere.getLength(lineString);
                                if (distance < minDistance) {
                                    closestFeature = feature;
                                    minDistance = distance;
                                }
                            }

                            for (const line of lines) {
                                if (closestFeature.getProperties().lignes_pas.includes(line)) {
                                    let dataToSend = {
                                        label: 'line',
                                        data: line
                                    }
                                    window.ReactNativeWebView.postMessage(JSON.stringify(dataToSend));

                                    dataToSend = {
                                        label: 'startStop',
                                        data: {
                                            name: closestFeature.getProperties().nom_generi,
                                            type: closestFeature.getProperties().categ_arret
                                        }
                                    }
                                    window.ReactNativeWebView.postMessage(JSON.stringify(dataToSend));
                                    
                                    featureStartStop.getGeometry().setCoordinates(ol.proj.fromLonLat(closestFeature.getGeometry().getCoordinates()));
                                    featureStartStop.set('name', closestFeature.getProperties().nom_generi);
                                    break;
                                }
                            }
                            showPath(closestFeature.getGeometry().getCoordinates());
                        });
                    } else {
                        alert('Erreur ' + response.status);
                    }
                })
                    .catch(function(error) {
                        alert('Erreur ' + error.message);
                    });
            }
            
            const featurePathStyle = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    width: 5, color: 'blue'
                })
            });

            const featurePath = new ol.Feature({
                geometry: new ol.geom.LineString([[0, 0], [0, 0]])
            });
            featurePath.setStyle(new ol.style.Style(null));
            vectorSource.addFeature(featurePath);

            function showPath(startStopLocation) {
                const request = 'https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?' +
                    'resource=bdtopo-osrm&profile=pedestrian&optimization=shortest&' +
                    'start=' + position_user[0] + ',' + position_user[1] +
                    '&end=' + startStopLocation[0] + ',' + startStopLocation[1] +
                    '&geometryFormat=polyline&getSteps=true';
                fetch(request).then(function(response) {
                    if(response.ok) {
                        response.json().then(function(data) {
                            const route = new ol.format.Polyline().readGeometry(data.geometry, {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:4326'
                            });
                            
                            let coords = [];
                            for (let coord of route.getCoordinates()) {
                                coord = ol.proj.fromLonLat(coord)
                                coords.push(coord)
                            }
                            route.setCoordinates(coords);

                            featurePath.setGeometry(route);

                            const dataToSend = {
                                label: 'steps',
                                data: data.portions[0].steps
                            }
                            
                            window.ReactNativeWebView.postMessage(JSON.stringify(dataToSend));

                            showResult();
                            zoomOnResult(startStopLocation);
                        });
                    } else {
                        alert('Erreur ' + response.status);
                    }
                })
                    .catch(function(error) {
                        alert('Erreur ' + error.message);
                    });
            }
            
            const stopStyle = new ol.style.Style({
                            image: new ol.style.Circle({
                                radius: 10,
                                fill: new ol.style.Fill({color: 'red'}),
                                stroke: new ol.style.Stroke({
                                    color: '#fff',
                                    width: 2
                                })
                            }),
                            text: new ol.style.Text({
                                font: '12px Calibri,sans-serif',
                                overflow: true,
                                fill: new ol.style.Fill({
                                    color: '#000'
                                }),
                                stroke: new ol.style.Stroke({
                                    color: '#fff',
                                    width: 3
                                }),
                                offsetY: -20
                            })
                        });
                        
            function showResult() {
                // affiche le chemin entre l'utilisateur et l'arrêt de départ
                featurePath.setStyle(featurePathStyle);
                
                const startStopStyle = stopStyle.clone();
                startStopStyle.getText().setText(featureStartStop.get('name'))
                
                function startStopStyleFunction() {
                    return [startStopStyle]
                }
                featureStartStop.setStyle(startStopStyleFunction);
                
                const endStopStyle = startStopStyle.clone();
                endStopStyle.getText().setText(featureEndStop.get('name'))
                function endStopStyleFunction() {
                    return [endStopStyle]
                }
                featureEndStop.setStyle(endStopStyleFunction)
            }
            
            function zoomOnResult(startStopLocation) {
                const request = 'https://wxs.ign.fr/calcul/geoportail/itineraire/rest/1.0.0/route?' +
                    'resource=bdtopo-osrm&profile=pedestrian&optimization=shortest&' +
                    'start=' + position_user[0] + ',' + position_user[1] +
                    '&end=' + endStopLocation[0] + ',' + endStopLocation[1] +
                    '&intermediates=' + startStopLocation[0] + ',' + startStopLocation[1] +
                    '&geometryFormat=polyline&getSteps=false';
                fetch(request).then(function(response) {
                    if(response.ok) {
                        response.json().then(function(data) {
                            let bbox = [];
                            for(let i= 0; i < data.bbox.length; i+=2) {
                                let coords = ol.proj.fromLonLat([data.bbox[i], data.bbox[i+1]]);
                                bbox = bbox.concat(coords);
                            }
                            bbox[0] -= 450;
                            bbox[1] -= 1600;
                            bbox[2] += 450;
                            bbox[3] += 450;
                            map.getView().fit(bbox, map.getSize());
                        });
                    } else {
                        alert('Erreur ' + response.status);
                    }
                })
                .catch(function(error) {
                    alert('Erreur ' + error.message);
                });
            }
            
            closer.onclick = function () {
                overlay.setPosition(undefined);
                closer.blur();
                return false;
            };
            map.on('singleclick', function(evt) {
                // On déclare l'url de la couche WMS
                
                var urlArret = layerArret.getSource().getFeatureInfoUrl(
                    evt.coordinate,
                    map.getView().getResolution(),
                    map.getView().getProjection(), {
                        // retour en format json 'INFO_FORMAT': 'application/json',
                    });
               
               var urlCom = layerCom.getSource().getFeatureInfoUrl(
                   evt.coordinate,
                   map.getView().getResolution(),
                   map.getView().getProjection(), {
                   });
                // Si l'url de la couche wms est valide, on envoie ces informations dans la div info
                function getInfo(url, datatype){
                    if(url){
                        var request = new XMLHttpRequest()
                        request.onreadystatechange = alertContents;
                        request.open('GET', url, true)
                        request.send()
                        function alertContents() {
                            if (request.readyState === XMLHttpRequest.DONE) {
                                if (request.status === 200) {
                                    const json = request.responseText;
                                    const obj = json.split('\\n');
                                    if (obj.length != 2){
                                        const coordinate = evt.coordinate;
                                        if (datatype == 1)
                                            content.innerHTML = '<code >' +data(obj,"nom_generi")+ '<br>'+ data(obj,"lignes_pas")+ '</code>';
                                        else
                                            content.innerHTML = '<code >' +data(obj,"message")+ '</code>';               
                                        overlay.setPosition(coordinate);
                                    }
                                     else{
                                         overlay.setPosition(undefined);
                                         closer.blur();
                                     }
                                }
                            }
                        }
                    }
                }
                getInfo(urlArret, 1);    
                getInfo(urlCom, 2);    
            });
            
            function data(obj,text){
                for (i = 0; i < obj.length; i++){
                    const data = obj[i].split('=')
                    if (data[0].substring(0, data[0].length-1) === text) {
                        return data[1].substring(1, data[1].length)
                    }
                }
            }
        </script>
    </body>
</html>`;

    return (
        <>
            <WebView
                ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
                originWhitelist={['*']} source={{html}} onMessage={onMessage} javaScriptEnabled={true} />
            {
                Object.keys(steps[0]).length == 0?
                    null
                    :
                    <View style={{position: 'absolute', bottom: 0, left: 0, right: 0, backgroundColor: 'white', height: panelClosed? '6%' : '40%'}}>
                        <AntDesign.Button name={panelClosed? 'up' : 'down'}
                                          size={24}
                                          color="black"
                                          style={{backgroundColor: 'white', justifyContent: 'center'}}
                                          onPress={() => setPanelClosed(prev => !prev)}  />
                        <FlatList data={steps}
                                  keyExtractor={(item,index) => index.toString()}
                                  renderItem={({item}) => (
                                      <View style={{display: 'flex', flexDirection: 'row', padding: 5, alignItems: 'center'}}>
                                          {
                                              item.icon == "MaterialCommunityIcons"?
                                                  <MaterialCommunityIcons name={item.iconName} size={24} color="black" />
                                                  :
                                                  <Feather name={item.iconName} size={24} color="black" />
                                          }
                                          <Text style={{flexShrink: 1, marginLeft: 5, fontSize: 17}}>{item.message}</Text>
                                      </View>
                                  )}
                        />
                    </View>
            }
        </>
);
}
