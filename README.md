# Transports en commun orléans

**date:** novembre à janvier  
**principe:** Utiliser un système de cartographie  
**sujet:** [PDF](sujet.pdf)

---

## Table des matières

[[_TOC_]]

<br/><br>

## Installation

### Téléchargement du projet

```sh
$ git clone https://gitlab.com/univ-orleans/m2/sig/transports-en-commun-orleans.git
```

### Installation des dépendances

```sh
$ cd transports-en-commun-orleans/
$ sudo npm install -g expo-cli
$ npm install
```

<br/><br>

## Execution du projet

```sh
$ npm start
```

<br/><br>

## Prérequis logiciel

| Logiciel   | Version | Documentation                                  | Description                                                    |
| :--------- | ------: | :--------------------------------------------- | :------------------------------------------------------------- |
| Node.js    | 16.13.1 | [nodejs.org](https://nodejs.org/)              | Serveur javascript                                             |
| Npm        |   8.1.2 | [npmjs.com](https://www.npmjs.com)             | Gestionnaire de paquets de Node.js                             |
| expo-cli   |   5.0.1 | [expo.dev](https://docs.expo.dev)              | Framework de développement multi-plateformes sous React Native |
| OpenLayers |   5.3.0 | [openlayers.org](https://openlayers.org)       | Affichage dynamique des cartes                                 |
| Geoserver  | 2.5.5.1 | [geoserver.org](http://geoserver.org)          | Serveur de données géospaciales                                |
| Postgis    |   3.0.3 | [http://www.postgis.fr](http://www.postgis.fr) | Plugin PostgreSQL pour la gestion de données géospaciales      |

<br/><br>

## Contributeurs

- Arnaud ORLAY (@arnorlay) : Master 2 IMIS Informatique
- Marion JURÉ (@Marionjure) : Master 2 IMIS Informatique
- Nicolas ZHOU (@Zhou_Nicolas) : Master 2 IMIS Informatique
- My Nina HONG (@ninahg) : Master 2 IMIS Informatique
